"""
Coordinate ascent, according to

@article{metzler2007linear,
  title={Linear feature-based models for information retrieval},
  author={Metzler, D. and Bruce Croft, W.},
  journal={Information Retrieval},
  volume={10},
  number={3},
  pages={257--274},
  year={2007},
  publisher={Springer}
}
For a nice exposition on the multinomial manifold, and how points are
projected onto it,

and
@book{metzler2011feature,
  title={A Feature-Centric View of Information Retrieval},
  author={Metzler, D.},
  volume={27},
  year={2011},
  publisher={Springer}
}
where coordinate ascent is treated in a bit more detail. (Although the pseudo
code version is not without errors)
"""
import sys
import argparse
from sparse_io import SparseSet
from apply_linear_model import apply_linear_model
import eval_trec_run as trec_eval
from qrel import Qrel

DEBUG = False
def debug(msg, fout = sys.stderr):
	if DEBUG:
		print >> fout, msg

def score(model, qrel, feature_set, fun_metric, args):
	# prepend intercept of 0 (rank equivalence guaranteed)
	model = [0.0] + model
	run = apply_linear_model(model, feature_set)
	if args.year == "2011":
		run = trec_eval.transform_run_to_2011_run(run)
	s = fun_metric(qrel, run)
	return s

def main(argv):

	global DEBUG

	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument("-d", "--debug", action="store_true")
	parser.add_argument("qrel",
			help="A TREC qrel file")
	parser.add_argument("feature_set",
			help="The data you wish to optimize a linear model on.")
	parser.add_argument("metric",
			choices=["MAP", "P30", "P20", "P10", "Rprec", "MRR"],
			help="The metric to optimize for")
	parser.add_argument("year", choices=["2011", "2012"],
			help="Evaluate 2011 or 2012 style")
	parser.add_argument("steps",
			type=int,
			help="""
steps >= 2. The number of steps performed in one-dimensional line searches.
Note that only positive weights for each feature will be considered.""")
	parser.add_argument("epsylon",
			type=float,
			help="""
epsylon >= 0.0. Bigger means stopping faster, being supoptimal, but avoiding
overfitting. Smaller means finding local maxima more precisely, stopping later.
metzler2011feature use e=0.0001""")
	parser.add_argument("--no_manifold",
			action="store_true",
			help=\
"""Do not project points on the manifold after each line search.""")
	parser.add_argument("--initialization",
			choices=["random", "uniform"],
			default="uniform",
			help=\
"""How to initialize the weight vector""")
	args = parser.parse_args(argv[1:])

	metric2fun = {\
			"MAP" : trec_eval.MAP,
			"P30" : trec_eval.P30,
			"P20" : trec_eval.P20,
			"P10" : trec_eval.P10,
			"Rprec" : trec_eval.Rprec,
			"MRR" : trec_eval.MRR}
	fun_metric = metric2fun[args.metric]

	if args.epsylon < 0.0:
		print >> sys.stderr, "Please supply a non-negative epsylon (>= 0)"
		return 2

	if args.steps < 2:
		print >> sys.stderr, "Please supply a number of steps >= 2"
		return 2

	if args.debug:
		DEBUG = True

	qrel = Qrel(args.qrel)
	feature_set = SparseSet(args.feature_set)
	n_features = feature_set.max_feat_idx()

	# initialize model
	if args.initialization == "uniform":
		current_model = [ 1.0 / n_features ] * n_features
	else: # random
		raise NotImplementedError


	def _score(model):
		return score(model, qrel, feature_set, fun_metric, args)

	debug("%.12e\t%s" % (_score(current_model),
			":".join(["%.6e" % (x,) for x in current_model])))

	# repeat until convergence
	while True:

		# copy current model into previous one for comparison later
		previous_model = current_model[:]

		# do a line search for each feature, manipulating current model
		for feat in range(n_features):

			# do line search, updating current model

			debug("Line search feature %d" % (feat,))

			best_model = None
			best_score = float("-inf")
			for x in range(args.steps):

				current_model[feat] = x / float(args.steps - 1)
				current_score = _score(current_model)
				if current_score > best_score:
					best_model = current_model[:]
					best_score = current_score

					debug("%.12e\t%s" % (best_score,
							":".join(["%.6e" % (x,) for x in best_model])))

			# for the next feature's line search, or, if they are all done, for
			# the next repeat of the whole process, set current model to best
			# model.
			current_model = best_model[:]

			# optionally project current_model back onto manifold NB:
			# implemented in this way it does not make much sense: I think the
			# pseudo code in metzler2011features has some errors.
			if not args.no_manifold:
				W = sum(current_model)
				for feat_j in range(n_features):
					current_model[feat_j] = current_model[feat_j] / W

		# check convergence
		diff = _score(current_model) - _score(previous_model)

		debug("Improvement: %.12e" % (diff,))

		if diff < args.epsylon:
			break

	# now write the current model to stdout, prepending the 0.0 intercept
	print "\n".join(["%.12e" % (0.0,)] + ["%.12e" % (x,) for x in current_model])

	return 0

if __name__ == "__main__":
	sys.exit(main(sys.argv))
