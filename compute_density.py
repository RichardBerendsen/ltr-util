"""
Compute density of a training set.
"""

import sys
import re
import sparse_io as io
import numpy as np


"""
computes density of matrix, defined as average percentage of empty entries in
a query feature matrix.
"""
def compute_density(t):

	percentages = [0.0] * t.num_queries()
	for i, (_, (_, _, feat_vectors)) in enumerate(t.queries()):
		for x in feat_vectors:
			percentages[i] += \
					len(x) # features idxs run from 1
		percentages[i] /= len(feat_vectors) * t.max_feat_idx()

	return np.mean(percentages), np.std(percentages), t.num_queries()


"""
compute_density.py feature_set

Will compute density of a training or test set
"""
def main(argv):

	if len(argv) != 2:
		print >> sys.stderr, __doc__
		return 2


	path_set = argv[1]

	t = io.SparseSet(path_set)
	mean, std, n = compute_density(t)

	print "Matrix: %s" % (path_set,)
	print "Number of queries: %d" % (n,)
	print "Avg density of a query: %.12e" % (mean,)
	print "Std of density of a query: %.12e" % (std,)


	return 0


if __name__ == "__main__":
	sys.exit(main(sys.argv))
