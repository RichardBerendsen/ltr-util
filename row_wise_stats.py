"""
%s stat [stat ...]

Given a matrix:
qid docid   val1    val2    ...

and stats, where stat is one of
    avg
    med
    max
    min
    nnz (number non zero)

produce output:
qid docid   stat    stat    stat

where stats are ordered as on the command line.
"""
import getopt
import sys
import numpy as np
import collections

def nnz(x):
    nnz_ = 0
    for i in x:
        if not np.allclose(i, 0.0):
            nnz_ += 1
    return nnz_

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "qh")
    except getopt.GetOptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2


    if len(args) == 0:
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2

    for o, a in opts:
        if o == "-h":
            print __doc__ % (argv[0],)
            return 0
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)

    s2func = {
            "avg":np.mean,
            "max":np.max,
            "min":np.min,
            "med":np.median,
            "nnz":nnz
    }

    if set(args) - set(s2func.keys()):
        print >> sys.stderr, "%s: Please choose supported stats" % (argv[0],)
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2

    for l in sys.stdin:
        parts = l.strip().split()
        qid, docid = parts[:2]
        x = np.array([np.float32(y) for y in parts[2:]])
        stats = []
        for stat in args:
            stats.append(s2func[stat](x))
        print "\t".join([qid, docid] + ["%.12e" % y for y in stats])


    return 0




if __name__ == "__main__":
    sys.exit(main(sys.argv))
