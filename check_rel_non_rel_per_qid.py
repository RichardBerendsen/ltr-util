import sys
import collections

qid2rel_non_rel = collections.defaultdict(lambda: [0, 0])

for l in sys.stdin:

    parts = l.strip().split()
    rel_str, qid_str = parts[:2]
    rel = int(rel_str)
    qid = int(qid_str.split(':')[1])

    if rel == 1:
        qid2rel_non_rel[qid][0] += 1
    elif rel == -1:
        qid2rel_non_rel[qid][1] += 1
    else:
        raise ValueError("invalid relevance value")


for qid, (rel, non_rel) in qid2rel_non_rel.iteritems():
    print "%d\t%d\t%d" % (qid, rel, non_rel)

