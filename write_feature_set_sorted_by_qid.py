"""
write sorted by qid
"""

import sparse_io
import sys

if len(sys.argv) != 2:
	print >> sys.stderr, "Please specify feature set"
	sys.exit(2)

path_feature_set = sys.argv[1]

s = sparse_io.SparseSet(path_feature_set)
s.write()
