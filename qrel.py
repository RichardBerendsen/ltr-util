"""
Class qrel

for reading in and storing a qrel file.
"""
import re
import sys
from collections import OrderedDict

class Qrel:

	def __init__(self, path):
		self.read(path)
		self._binary = None


	def read(self, path):
		rws = re.compile(r"\s+")

		self._qid2docid2rel = OrderedDict()

		f = open(path)
		for l in f:
			qid, _, docid, rel = re.split(rws, l.strip())
			rel = int(rel)

			try:
				self._qid2docid2rel[qid][docid] = rel
			except KeyError:
				self._qid2docid2rel[qid] = {docid : rel}

		f.close()

	"""
	return a set of documents with rel > 0 per query
	"""
	def binary(self):
		if self._binary is None:
			self._compute_binary()
		return self._binary

	def _compute_binary(self):
		self._binary = OrderedDict()
		for qid, docids in self._qid2docid2rel.iteritems():
			self._binary[qid] = frozenset(\
					[docid for docid, rel in docids.iteritems() if rel > 0])

	def write(self, fout):
		for qid, docid2rel in self._qid2docid2rel.iteritems():
			for docid, rel in docid2rel.iteritems():
				print >> fout, "%s 0 %s %d" % (\
						qid, docid, rel)

def main(argv):
	if len(argv) != 2:
		print >> sys.stderr, "qrel.py qrel"
		return 2

	qrel = Qrel(sys.argv[1])
	qrel.write(sys.stdout)

	return 0


if __name__ == "__main__":
	sys.exit(main(sys.argv))
