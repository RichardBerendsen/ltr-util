"""
%s feat

Linearly normalize feat.

Assume scores >= 0. (raises ValueError if this is violated)
Assume if an item is not there, it's score was 0. (so this program takes sparse
    input)
Normalize by dividing by the maximum score.

Options:

    -q Normalize per query, instead of over all queries.
    -h Display this help
"""
import getopt
import sys
import numpy as np
import collections

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "qh")
    except getopt.GetOptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2


    if len(args) != 1:
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2

    per_query = False
    for o, a in opts:
        if o == "-h":
            print __doc__ % (argv[0],)
            return 0
        elif o == "-q":
            per_query = True
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)


    feat_path = args[0]

    if per_query:
        normalize_per_query(feat_path)
    else:
        normalize(feat_path)

    return 0



def normalize_per_query(feat_path): # {{{
    # Go through file once for min and max
    f = open(feat_path)
    # peek ahead for number of colums
    n = len(f.readline().strip().split()) - 2
    f.seek(0)
    if n:
        qid2max = collections.defaultdict(lambda: np.array([[float("-inf")] * n]))
    else:
        raise ValueError("Not enough columns found in data")

    for l in f:

        # ignore rank, work with score
        parts = l.strip().split()
        qid, docid = parts[:2]
        x = np.array([[np.float(y) for y in parts[2:]]])

        if np.min(x) < np.float(0.0):
            raise ValueError("Negative score in input, exiting")

        qid2max[qid] = np.array([np.max(\
                np.concatenate((qid2max[qid], x), axis=0), axis=0)])

    # if there is a 0 in max_x, it means all values were 0.0, and we want them
    # to stay 0.0, so we'll divide by 1
    qid2max_safe = {}
    for qid, max_x in qid2max.iteritems():
        qid2max_safe[qid] = np.array([1.0 if np.allclose(0.0, y) else y for y in max_x])


    # Go through file a second time to normalize.
    f.seek(0)
    for l in f:
        parts = l.strip().split()
        qid, docid = parts[:2]
        x = np.array([[np.float(y) for y in parts[2:]]]) / qid2max_safe[qid]

        print "\t".join([qid, docid] + ["%.12e" % (y,) for y in x[0]])

#}}}
def normalize(feat_path): # {{{

    # Go through file once for min and max
    f = open(feat_path)
    # peek ahead for number of columns
    n = len(f.readline().strip().split()) - 2
    f.seek(0)
    if n:
        max_x = np.array([[float("-inf")] * n])
    else:
        raise ValueError("Not enough columns found in data")

    for l in f:

        # ignore rank, work with score
        parts = l.strip().split()
        qid, docid = parts[:2]
        x = np.array([[np.float(y) for y in parts[2:]]])

        if np.min(x) < np.float(0.0):
            raise ValueError("Negative score in input")

        max_x = np.array([np.max(np.concatenate((max_x, x), axis=0), axis=0)])

    # if there is a 0 in max_x, it means all values were 0.0, and we want them
    # to stay 0.0, so we'll divide by 1
    max_x = np.array([1.0 if np.allclose(0.0, y) else y for y in max_x])

    # Go through file a second time to normalize.
    f.seek(0)
    for l in f:
        parts = l.strip().split()
        qid, docid = parts[:2]
        x = np.array([[np.float(y) for y in parts[2:]]])
        x = x / max_x

        print "\t".join([qid, docid] + ["%.12e" % (y,) for y in x[0]])
#}}}

if __name__ == "__main__":
    sys.exit(main(sys.argv))
