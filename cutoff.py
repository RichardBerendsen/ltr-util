"""
cutoff.py

GPL3, Nov 2012
Richard Berendsen

cutoff.py K < TREC_run

    Will keep only the top K (by retrieval score, and, in case of ties, by
    docid) of TREC_run for each query.

Options:

    -h Show this help
"""

import sys
import re
import os
import getopt
import collections
import numpy as np
import itertools


def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "h")
    except getopt.GetoptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__
        return 2



    # catch conflicting options

    # defaults
    sep = r"\s+"
    sep_out = "\t"
    for o, a in opts:
        if o == "-h":
            print __doc__
            return 0
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)
            return 2

    if len(args) != 1:
        print >> sys.stderr, __doc__
        return 2

    K = int(args[0])

    # qid 2 [(score_flt, docid_int, context = (Q0, docid_str, rank, score_str, runid))]
    qid2docs = collections.defaultdict(lambda: [])

    # store stuff
    for l in sys.stdin:
        qid, Q0, docid_str, rank, score_str, runid = re.split(sep, l.strip())
        score_flt = float(score_str)
        qid2docs[qid].append((score_flt, docid_str,
                (Q0, docid_str, rank, score_str, runid)))

    # sort stuff and print it out again.
    for qid, docs in qid2docs.iteritems():
        # note that bigger scores should sort higher.
        # and bigger docids should sort higher, too.
        # so we can sort both fields reversed.
        sorted_docs = sorted(docs, reverse=True)

        # now cutoff at K and print
        for doc in sorted_docs[:K]:
            print sep_out.join((qid,) + doc[2])


    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
