"""
sparse training or test set I/O.
"""

import sys
import re
import collections
import numpy as np

"""
reads in a training or test set from path and returns it as a dict of qid to a
list of document vectors in the form of dictionaries from feature id to score

NB: an optional docid field can be present in the comment string, e.g.
# docid:blaat hoedatzo
will register as 'blaat', because blaat is a series of non-whitespace characters
following docid:

NB: the qid field must be numeric, as this is requested by svm_rank_learn, and,
moreover, it must be sorted increasingly (why!?). So, we will require it to be
int. And, we will write it sorted increasingly (but not complain when the input
is not). However, internally, we will store qid as a string. It does not make
sense to induce some kind of ordering on them.
"""
class SparseSet:

    DOCIDS = 0
    LABELS = 1
    FEATS = 2

    def __init__(self, path = None, support_normalization = False):
        self._t = collections.OrderedDict()
        self._max_feat_idx = 0
        self._feat_idxs = set()
        self._support_normalization = support_normalization
        self._N = 0
        self._feat2max_value = collections.defaultdict(\
                lambda: 0.0)
        self._qid2feat2max_value = collections.defaultdict(\
                lambda: collections.defaultdict(\
                lambda: 0.0))
        if not path is None:
            if self._support_normalization:
                self.read_and_bookkeep(path)
            else:
                self.read(path)

    def read(self, path):
        rws = re.compile(r"\s+")
        rc = re.compile(r":")
        rh = re.compile(r"#")
        rdocid = re.compile(r"docid:([^\s]+)")

        f_set = open(path)

        for l in f_set:

            content, comment = re.split(rh, l, maxsplit = 1)
            parts = re.split(rws, content.strip())
            label = parts[0]
            qid = str(int(re.split(rc, parts[1])[1]))
            docid_match = re.search(rdocid, comment)
            if docid_match is not None:
                docid = docid_match.group(1)
            else:
                docid = None
            try:
                self._t[qid][SparseSet.DOCIDS].append(docid)
                self._t[qid][SparseSet.LABELS].append(label)
                self._t[qid][SparseSet.FEATS].append({})
            except KeyError:
                self._t[qid] = ([docid], [label], [{}])
            for feat in parts[2:]:
                idx, val = feat.split(":")
                idx = int(idx)
                val = float(val)
                if val < 0.0:
                    raise ValueError("Negative value in feature set")
                if not np.allclose(0.0, val):
                    self._feat_idxs.add(idx)
                    self._t[qid][SparseSet.FEATS][-1][idx] = val
            self._N += 1

        f_set.close()

    def read_and_bookkeep(self, path):
        rws = re.compile(r"\s+")
        rc = re.compile(r":")
        rh = re.compile(r"#")
        rdocid = re.compile(r"docid:([^\s]+)")

        f_set = open(path)

        for l in f_set:

            content, comment = re.split(rh, l, maxsplit = 1)
            parts = re.split(rws, content.strip())
            label = parts[0]
            qid = str(int(re.split(rc, parts[1])[1]))
            docid_match = re.search(rdocid, comment)
            if docid_match is not None:
                docid = docid_match.group(1)
            else:
                docid = None
            try:
                self._t[qid][SparseSet.DOCIDS].append(docid)
                self._t[qid][SparseSet.LABELS].append(label)
                self._t[qid][SparseSet.FEATS].append({})
            except KeyError:
                self._t[qid] = ([docid], [label], [{}])
            for feat in parts[2:]:
                idx, val = feat.split(":")
                idx = int(idx)
                val = float(val)
                if val < 0.0:
                    raise ValueError("Negative value in feature set")
                if not np.allclose(0.0, val):
                    self._feat_idxs.add(idx)
                    if val > self._feat2max_value[idx]:
                        self._feat2max_value[idx] = val
                    if val > self._qid2feat2max_value[qid][idx]:
                        self._qid2feat2max_value[qid][idx] = val
                    self._t[qid][SparseSet.FEATS][-1][idx] = val
            self._N += 1

        f_set.close()

    """
    writes out a training or test set
    """
    def write(self, fout=sys.stdout):
        for qid, (docids, labels, feats) in sorted(self.queries(),
                key=lambda x: int(x[0])):
            for docid, label, feat in zip(docids, labels, feats):
                if docid is None:
                    print >> fout, "%s qid:%s %s" % (label, qid, \
                            " ".join(["%d:%.12e" % (idx, val) \
                            for (idx, val) in sorted(feat.iteritems()) \
                            if not np.allclose(0.0, val)]))
                else:
                    print >> fout, "%s qid:%s %s#docid:%s" % (label, qid, \
                            " ".join(["%d:%.12e" % (idx, val) \
                            for (idx, val) in sorted(feat.iteritems())\
                            if not np.allclose(0.0, val)]),
                            docid)

    def num_queries(self):
        return len(self._t)

    def queries(self):
        return self._t.iteritems()

    def qids(self):
        return self._t.keys()

    def max_feat_idx(self):
        return max(self._feat_idxs)

    def add_query(self, qid):
        self._t[qid] = ([], [], [])

    def add_query_with_value(self, qid, value):
        self._t[qid] = value

    def del_query(self, qid):
        del self._t[qid]

    def pop_query(self, qid):
        return self._t.pop(qid)

    def add_document(self, qid, docid, label, feats):
        self._t[qid][SparseSet.DOCIDS].append(docid)
        self._t[qid][SparseSet.LABELS].append(label)
        self._t[qid][SparseSet.FEATS].append(feats)

    """
    assumes minimum is 0. Divides feature values of each feature by the
    maximum value of that feature.

    What if the maximum value of a feature is 0.0? Then apparently
    the feature does not change for any observation. It makes sense,
    then, to keep it zero.
    """
    def normalize_linear(self, per_query = True, feat_idxs = None):
        if not self._support_normalization:
            raise NotImplementedError(\
                    "Not constructed with support for normalization")

        if feat_idxs is None:
            feat_idxs = self._feat_idxs
        else:
            feat_idxs = set(feat_idxs)

        if per_query:
            def max_value(qid, feat):
                return self._qid2feat2max_value[qid][feat]
        else:
            def max_value(_, feat):
                return self._feat2max_value[feat]

        t = {} # we have to make a copy, because we cannot modify
               # what we iterate over.
        for qid, (docids, labels, feat_vectors) in self._t.iteritems():
            t[qid] = (docids, labels, [])
            for feat_vector in feat_vectors:
                t[qid][SparseSet.FEATS].append({})
                for feat, val in feat_vector.iteritems():
                    if val < 0.0:
                        raise ValueError("Unexpected negative value in feature set")
                    if feat in feat_idxs:
                        max_val = max_value(qid, feat)
                        if max_val > 0.0:
                            t[qid][SparseSet.FEATS][-1][feat] = val \
                                    / max_val
                        else:
                            pass
                            # do not store the feature value, it
                            # is zero and it is a sparse matrix.
                    else:
                        t[qid][SparseSet.FEATS][-1][feat] = val
        del self._t # hopefully free some memory immediately
        self._t = t

def main(argv):
    # read in sparse training or test set and write it out
    assert(len(argv) == 2)
    t = SparseSet(argv[1])

    # write it out
    t.write()

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
