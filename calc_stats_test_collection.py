"""
Usage:
    calc_stats_test_collection.py name qrel queries

Where name is the name of the test collection,
qrel is a usual TREC qrel file. It will be seen as a binary
qrel file here, where rel=1 iff rel_qrel > 0.
queries has the format:

qid\tterm_1\tterm_2, etc.

Will print out
        name
        number of queries
        max n. words in query
        min n. words in query
        avg n. words in query
        number of reldocs
        max n. of reldocs for one query
        min n. of reldocs for one query
        avg n. of reldocs for one query
separated by tabs, on a single line
"""

import sys
from qrel import Qrel

if len(sys.argv) != 4:
    print >> sys.stderr, __doc__
    sys.exit(2)

name = sys.argv[1]
path_qrel = sys.argv[2]
path_queries = sys.argv[3]

# do qrel first, we only want stats on queries
# that are also in the qrel
qrel = Qrel(path_qrel).binary()
qids_from_qrel = frozenset(qrel.keys())
n_reldocs = 0
max_n_reldocs = float("-inf")
min_n_reldocs = float("inf")
for qid, reldocs in qrel.iteritems():
    n = len(reldocs)
    n_reldocs += n
    if n > max_n_reldocs:
        max_n_reldocs = n
    if n < min_n_reldocs:
        min_n_reldocs = n
avg_n_reldocs = n_reldocs / float(len(qrel))

qids_from_queries = set()
n_terms = 0
max_n_terms = float("-inf")
min_n_terms = float("inf")
f = open(path_queries)
for l in f:
    l = l.strip()
    if l == "":
        continue
    parts = l.split("\t")
    qid = parts[0]
    if qid in qids_from_qrel:
        qids_from_queries.add(qid)
        terms = parts[1:]
        n = len(terms)
        n_terms += n
        if n > max_n_terms:
            max_n_terms = n
        if n < min_n_terms:
            min_n_terms = n
    else:
        print >> sys.stderr, "Notice: qid %s not in qrel" % (qid,)
f.close()
if not qids_from_qrel == qids_from_queries:
    print >> sys.stderr, "Warning: not all qids from qrel in queries"
avg_n_terms = n_terms / float(len(qids_from_queries))


print "%s\t%d\t%d\t%d\t%.1f\t%d\t%d\t%d\t%.1f" % (
        name,
        len(qids_from_queries), max_n_terms, min_n_terms, avg_n_terms,
        n_reldocs, max_n_reldocs, min_n_reldocs, avg_n_reldocs)





