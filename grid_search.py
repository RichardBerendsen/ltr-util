"""
Grid search, according to

@article{metzler2007linear,
  title={Linear feature-based models for information retrieval},
  author={Metzler, D. and Bruce Croft, W.},
  journal={Information Retrieval},
  volume={10},
  number={3},
  pages={257--274},
  year={2007},
  publisher={Springer}
}
(for a nice exposition on the multinomial manifold),

and
@book{metzler2011feature,
  title={A Feature-Centric View of Information Retrieval},
  author={Metzler, D.},
  volume={27},
  year={2011},
  publisher={Springer}
}
for a nice exposition of grid search over the multinomial manifold (doing a
	grid search over the original parameter space is unbounded and ill defined,
	according to metzler2011feature. I don't really see that, but certainly
	doing a search over the manifold is more efficient and rank equivalent,
	too.)

Architecture, in pseudo code, after metzler2011feature.

We have a parameter K, that controls how fine the grid is.
The stepsize will be e = 1/K. Now we define the grid G:

G = { L = (k1*e, k2*e, ..., kd*e) : sum_i ki = K }

Where ki is a natural number. In other words, the grid G
is the set of all linear combinations that sum up to one
and that can be created using only points that are on a
block grid with step size e. The key point of metzler2007linear
is that we can simply try all weight vectors that sum up to one.
And then we will surely hit upon one linear feature based model,
that is, all linear feature based models are rank equivalent to one
of these. Thus we can find the optimal linear model.

The pseudo code in metzler2011feature is a bit confusing due to reuse of
indexes in while loops. Let's give it a try ourselves. E equals an evaluation
score, for example mean average precision. Higher is better in this pseudocode.

A recursive solution is by far the easiest here.  But Python could run out of
stack quite fast. Instead, we'll leave it to the libraries.  When we think of
each dimension as a bin, we can say that we are allowed to sample a grand total
of K balls from all of the bins.  We want to find all possible ways of sampling
in this way.  This is equivalent to finding all combinations of K from d
dimensions *with replacement* (so we do not worry about the intercept, or about
scaling)

So, we do:
import itertools

max_score =  -inf
max_K = None
for L in map(makeL, itertools.combinations_with_replacement(range(1,d), K)):
	score = evaluate(K)
	if score > max_score:
		max_score = score
		max_K = K

Note that itertools would generate sequences like 1112233, meaning makeL would
then make L look like (3, 2, 2)

Initial, slow plan:
The evaluation function will read in a test set, and using the weight vector K
will produce a run (writing it to disk in a temporary location) It will then
call bash scripts to handle the evaluation, and read from disk the scores
again. It will give back a single score, and this will be used to optimize.  So
we can evaluate the 2011 way, or the 2012 way. In the 2011 way, the run will
first be cut off at position 30, a state of the art technique. The evaluation
function will be a separate module, because it can also be used by the
coordinate ascent method, for example.

New plan:
Code evaluation metrics in Python afresh, and run the whole thing in memory.
This way, we only have to read in a feature set once. And we don't have to
write to disk any runs, evaluate them (which means reading them in again), and
read in scores. All of this is very useful, because a run can be quite big.
"""
import sys
import argparse
from sparse_io import SparseSet
from itertools import combinations_with_replacement
from apply_linear_model import apply_linear_model
import eval_trec_run as trec_eval
from qrel import Qrel

"""
L is a model, of length n_features + 1. It's first entry is
the intercept, which is always set to zero. The intercept is
required by apply_linear_model, but it's value does not matter,
since scores will be rank equivalent no matter the value.
To speed up calculations, we will also not scale L: again,
the values will be rank equivalent anyway.
"""
def makeL(idxs, n_features):
	debug("idxs: %s" % (idxs,))
	debug("n_features: %d" % (n_features,))
	L = [0] * (n_features + 1)
	for idx in idxs:
		L[idx] += 1
	return L

DEBUG = False
def debug(msg, fout = sys.stderr):
	if DEBUG:
		print >> fout, msg

def main(argv):

	global DEBUG

	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument("-d", "--debug", action="store_true")
	parser.add_argument("qrel",
			help="A TREC qrel file")
	parser.add_argument("feature_set",
			help="The data you wish to optimize a linear model on.")
	parser.add_argument("metric",
			choices=["MAP", "P30", "P20", "P10", "Rprec", "MRR"],
			help="The metric to optimize for")
	parser.add_argument("year", choices=["2011", "2012"],
			help="Evaluate 2011 or 2012 style")
	parser.add_argument("K",
			type=int,
			help="K > 0, bigger means a higher resolution grid")
	args = parser.parse_args(argv[1:])

	metric2fun = {\
			"MAP" : trec_eval.MAP,
			"P30" : trec_eval.P30,
			"P20" : trec_eval.P20,
			"P10" : trec_eval.P10,
			"Rprec" : trec_eval.Rprec,
			"MRR" : trec_eval.MRR}
	fun_metric = metric2fun[args.metric]

	if args.K <= 0:
		print >> sys.stderr, "Please supply a positive number for K"
		return 2

	if args.debug:
		DEBUG = True
	debug("Debugging is on")

	qrel = Qrel(args.qrel)
	feature_set = SparseSet(args.feature_set)
	n_features = feature_set.max_feat_idx()

	max_score = float("-inf")
	optimal_model = None

	for L in [makeL(x, n_features) for x in \
			combinations_with_replacement(range(1, n_features + 1), args.K)]:
			run = apply_linear_model(L, feature_set)
			if args.year == "2011":
				run = trec_eval.transform_run_to_2011_run(run)
			score = fun_metric(qrel, run)
			if score > max_score:
				max_score = score
				optimal_model = L
			debug("%.12e\t%s" % (score, ";".join(["%.6e" % (x,) for x in L])))

	# now write the optimal model to stdout.
	print "\n".join(["%.12e" % (x,) for x in optimal_model])

	return 0

if __name__ == "__main__":
	sys.exit(main(sys.argv))
