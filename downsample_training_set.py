"""
downsample_training_set.py training_set

Current downsamples negative training examples for each query such that a ratio
of 1:1 between positive and negative training examples is maintained.

Note that the positive training examples in the training form a subset of the
relevant documents according to the qrels: some relevant documents are not
retrieved by any algorithm, and some are not even in the preprocessed
collection. We take the number of positive training examples present in the
training set as our starting point.
"""

import sys
from random import sample
from sparse_io import SparseSet

if len(sys.argv) != 2:
	print >> sys.stderr, __doc__
	sys.exit(2)

training_set = SparseSet(sys.argv[1])
downsampled_training_set = SparseSet()


for qid, (docids, labels, feat_vecs) in training_set.queries():

	# add query to downsampled training set.
	downsampled_training_set.add_query(qid)

	rel = [(x, y, z) for (x, y, z) in zip(docids, labels, feat_vecs) if y == "1"]
	non_rel = [(x, y, z) for (x, y, z) in zip(docids, labels, feat_vecs) if y == "-1"]

	# if we have more relevant than non-relevant documents in our pool,
	# keep all non-relevant documents.

	if len(rel) >= len(non_rel):
		indices_sampled_non_rel = xrange(len(non_rel))
	else:
		indices_sampled_non_rel = sample(xrange(len(non_rel)), len(rel))

	for docid, label, feat_vec in rel:
		downsampled_training_set.add_document(\
				qid, docid, label, feat_vec)

	for i in indices_sampled_non_rel:
		docid, label, feat_vec = non_rel[i]
		downsampled_training_set.add_document(\
				qid, docid, label, feat_vec)


downsampled_training_set.write()
