"""
Functions for applying a linear model

Call as main? Usage:
	apply_linear_model.py model test_set run_id

Will apply model to a training or test set, producing a TREC run with run_id
"""
import sys
import re
import sparse_io as io
from trec_run import TrecRun

"""
X is a list of sparse feature vectors, each of which
is a dictionary of idx to value

model is a list of length >= max_feat_idx in X.

Returns a list of length len(X) with scores after
applying the model.
"""
def _apply_linear_model(model, X):
	y = [model[0]] * len(X)
	for i, x in enumerate(X):
		for idx, val in x.iteritems():
			y[i] += model[idx] * val
	return y

"""
Given a model (just a vector of floats) and a SparseSet,
return a TrecRun to which the model has been applied.
"""
def apply_linear_model(model, t, runid=None):
	assert(len(model) >= t.max_feat_idx())

	run = TrecRun(runid=runid)
	for qid, (docids, _, feat_vectors) in t.queries():
		run.add_query(qid, docids,
				_apply_linear_model(model, feat_vectors))

	return run

def _read_model(path_model):
	f = open(path_model)
	m = [float(x) for x in re.split(r"\s+", f.read().strip())]
	f.close()
	return m

"""
apply_linear_model.py model test_set run_id

Will apply model to a training or test set, producing a TREC run with run_id
"""
def main(argv):

	if len(argv) != 4:
		print >> sys.stderr, __doc__
		return 2


	path_model = argv[1]
	path_test_set = argv[2]
	runid = argv[3]

	model = _read_model(path_model)
	t = io.SparseSet(path_test_set)

	run = apply_linear_model(model, t, runid)
	run.write(sys.stdout)

	return 0


if __name__ == "__main__":
	sys.exit(main(sys.argv))
