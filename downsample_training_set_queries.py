"""
downsample_training_set_queries.py training_set qids
"""

import sys
from random import sample
from sparse_io import SparseSet

if len(sys.argv) != 3:
	print >> sys.stderr, __doc__
	sys.exit(2)

training_set = SparseSet(sys.argv[1])
f = open(sys.argv[2])
qids = frozenset([l.strip() for l in f.readlines()])
f.close()


all_qids = training_set.qids()[:] # copy, cause we want to manipulate training_set itself.
for qid in all_qids:
	if qid not in qids:
		training_set.del_query(qid)

training_set.write()
