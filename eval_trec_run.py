"""
Functions for evaluating a trec run

Such functions receive a run and qrel, and give back a score.
"""
import sys
from trec_run import TrecRun
from qrel import Qrel
import copy


"""
Cut run at rank 30, and then sort by docid instead
of by score.
"""
def transform_run_to_2011_run(run):
	run = copy.deepcopy(run)
	run.cutoff(30)
	transformed_run = TrecRun(runid=run.runid())

	# the following trick makes use of the fact that TrecRun sorts its docs by
	# score, reversed.  by replacing the scores with the docids, we will put
	# newer tweets first, achieving the desired effect.
	for qid, (docs, scores) in run.queries():
		transformed_run.add_query(qid,
				docs, docs)

	return transformed_run

"""
Precision at K for one query
"""
def _PatK(docs, reldocs, k):
	return len(frozenset(docs[:k]) & reldocs) / float(k)

"""
Average Precision

defined as

(1 / R) * sum_j(P@j * i(j))

where j runs over the ranked list, and i(j) is an indicator function
returning 1 if there is a relevant document at rank j and 0 otherwise.
R is the number of relevant documents.
"""
def _AP(docs, reldocs):
	R = float(len(reldocs))
	cumul = 0
	for j in range(len(docs)):
		if docs[j] in reldocs:
			cumul += _PatK(docs, reldocs, j + 1)
	return cumul / R

"""
Reciprocal Rank
"""
def _RR(docs, reldocs):
	rank_first_rel = float("inf")
	for i, doc in enumerate(docs):
		if doc in reldocs:
			rank_first_rel = float(i + 1)
			break
	return 1 / rank_first_rel

"""
Precision at K
"""
def PatK(qrel, run, k):
	qrel = qrel.binary()
	n_queries = len(qrel.keys())
	cumul = 0
	for qid, reldocs in qrel.iteritems():
		cumul += _PatK(run.docs(qid), reldocs, k)
	return cumul / float(n_queries)

"""
Mean Average Precision
"""
def MAP(qrel, run):
	qrel = qrel.binary()
	n_queries = len(qrel.keys())
	cumulative_ap = 0
	for qid, reldocs in qrel.iteritems():
		cumulative_ap += _AP(run.docs(qid), reldocs)
	return cumulative_ap / float(n_queries)

"""
Mean Reciprocal Rank
"""
def MRR(qrel, run):
	qrel = qrel.binary()
	n_queries = len(qrel.keys())
	cumul = 0
	for qid, reldocs in qrel.iteritems():
		cumul += _RR(run.docs(qid), reldocs)
	return cumul / float(n_queries)

"""
Average R-precision
"""
def Rprec(qrel, run):
	qrel = qrel.binary()
	n_queries = len(qrel.keys())
	cumul = 0
	for qid, reldocs in qrel.iteritems():
		cumul += _PatK(run.docs(qid), reldocs, len(reldocs))
	return cumul / float(n_queries)

"""
Convenience functions
"""
def P30(qrel, run):
	return PatK(qrel, run, 30)
def P20(qrel, run):
	return PatK(qrel, run, 20)
def P15(qrel, run):
	return PatK(qrel, run, 15)
def P10(qrel, run):
	return PatK(qrel, run, 10)
def P5(qrel, run):
	return PatK(qrel, run, 5)
def P1(qrel, run):
	return PatK(qrel, run, 1)

def main(argv):
	if len(argv) != 3:
		print >> sys.stderr, "eval_trec_run.py qrel run"
		return 2

	qrel_path = sys.argv[1]
	qrel = Qrel(qrel_path)

	run_path = sys.argv[2]
	f_run = open(run_path)
	run = TrecRun(fin=f_run)
	f_run.close()

	print "=== 2011 scores ==="
	run2011 = transform_run_to_2011_run(run)
	print "P30:%.12e" % (PatK(qrel, run2011, 30),)
	print "P20:%.12e" % (PatK(qrel, run2011, 20),)
	print "P10:%.12e" % (PatK(qrel, run2011, 10),)
	print "MAP:%.12e" % (MAP(qrel, run2011),)
	print "MRR:%.12e" % (MRR(qrel, run2011),)
	print "Rprec:%.12e" % (Rprec(qrel, run2011),)

	print
	print "=== 2012 scores ==="
	print "P30:%.12e" % (PatK(qrel, run, 30),)
	print "P20:%.12e" % (PatK(qrel, run, 20),)
	print "P10:%.12e" % (PatK(qrel, run, 10),)
	print "MAP:%.12e" % (MAP(qrel, run),)
	print "MRR:%.12e" % (MRR(qrel, run),)
	print "Rprec:%.12e" % (Rprec(qrel, run),)

	return 0

if __name__ == "__main__":
	sys.exit(main(sys.argv))
