import sys
import signal
import re
signal.signal(signal.SIGPIPE, signal.SIG_DFL)

if len(sys.argv) != 2:
	print >> sys.stderr, "please supply feature set."
	sys.exit(2)

f = open(sys.argv[1])

for l in f:
	content, qid, docid = l.split("#")
	label, qidstr, features = re.split(r"\s+", content.strip(), maxsplit=2)
	qid = str(int(qidstr.lstrip("qid:PT_")))
	print "%s qid:%s %s#docid:%s" % (label, qid, features, docid.strip())
