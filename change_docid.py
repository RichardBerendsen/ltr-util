import sys
import signal
signal.signal(signal.SIGPIPE, signal.SIG_DFL)

if len(sys.argv) != 2:
	print >> sys.stderr, "please supply feature set."
	sys.exit(2)

f = open(sys.argv[1])

for l in f:
	content, qid, docid = l.split("#")
	print "%s#docid:%s" % (content.strip(), docid.strip())
