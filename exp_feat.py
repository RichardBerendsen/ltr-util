"""
%s < feat

Take exponent scores of feat file.

feat:
qid docid   0.234   4.323   ...

"""
import sys
import getopt
import numpy as np

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "h")
    except getopt.GetOptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2


    if len(args) != 0:
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2

    for o, a in opts:
        if o == "-h":
            print __doc__ % (argv[0],)
            return 0
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)

    for l in sys.stdin:
        parts = l.strip().split()
        qid, docid = parts[:2]
        x = np.exp([float(y) for y in parts[2:]])
        print "\t".join([qid, docid] + ["%.12e" % (y,) for y in x])

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
