"""
write_train_or_test_set.py features

Will write a train/test set like this

1   qid:X   1:0.20  3:0.367 ... # qid # docid
...

feature must be a file of format

qid docid   0.12 0.79 ...
...

Options:

 -h Print this help
 -l labels
    Use labels in labels. labels should be a qrels file:
        qid 0 docid rel
        ...
    Where rel is an integer.
    Behavior:
        rel > 0:
            write a training label 1
        rel <= 0 or (qid,docid) not in qrel:
            write a training label -1

TODO FIXME XXX:
    this code does not write a sparse matrix in the sense
    that it does not check if a value is (close to) zero to
    refrain from putting it in.
"""

import sys
import re
import getopt
import collections

def load_labels_from(path, labels):
    f = open(path)
    for l in f:
        qid, _, docid, rel = l.strip().split()
        if int(rel) > 0:
            labels[qid].add(docid)
    f.close()


def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "hl:")
    except getopt.GetOptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__
        return 2

    if len(args) != 1:
        print >> sys.stderr, __doc__
        return 2

    path_features = args[0]

    # defaults
    load_labels = False
    path_labels = ""
    labels = collections.defaultdict(lambda: set())

    for o, a in opts:
        if o == "-h":
            print __doc__
            return 0
        elif o == "-l":
            load_labels = True
            path_labels = a
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)

    f = open(path_features)

    if load_labels:
        load_labels_from(path_labels, labels)

    for l in f:
        parts = re.split(r"\s+", l.strip())
        qid = parts[0]
        docid = parts[1]
        if not load_labels:
            label = "1"
        else:
            if docid in labels[qid]:
                label = "1"
            else:
                label = "-1"
        print "\t".join([label, "qid:%s" % (qid,)] + \
                ["%d:%s" % (i, x) for i, x in enumerate(parts[2:],
					start=1)] + ["#docid:%s" % (docid,)])



    f.close()

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
