"""
%s feat

Linearly normalize query feature.

Assume scores >= 0. (raises ValueError if this is violated)
Assume if an item is not there, it's score was 0. (so this program takes sparse
    input)
Normalize by dividing by the maximum score.

Options:

    -h Display this help
"""
import getopt
import sys
import numpy as np
import collections

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "h")
    except getopt.GetOptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2


    if len(args) != 1:
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2

    per_query = False
    for o, a in opts:
        if o == "-h":
            print __doc__ % (argv[0],)
            return 0
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)


    feat_path = args[0]

    normalize(feat_path)

    return 0



def normalize(feat_path): # {{{

    # Go through file once for min and max
    f = open(feat_path)
    # peek ahead for number of columns
    n = len(f.readline().strip().split()) - 1
    f.seek(0)
    if n:
        max_x = np.array([[float("-inf")] * n])
    else:
        raise ValueError("Not enough columns found in data")

    for l in f:

        # ignore rank, work with score
        parts = l.strip().split()
        qid = parts[0]
        x = np.array([[np.float(y) for y in parts[1:]]])

        if np.min(x) < np.float(0.0):
            raise ValueError("Negative score in input")

        max_x = np.array([np.max(np.concatenate((max_x, x), axis=0), axis=0)])

    # Go through file a second time to normalize.
    f.seek(0)
    for l in f:
        parts = l.strip().split()
        qid = parts[0]
        x = np.array([[np.float(y) for y in parts[1:]]])
        x = x / max_x

        print "\t".join([qid] + ["%.12e" % (y,) for y in x[0]])
#}}}

if __name__ == "__main__":
    sys.exit(main(sys.argv))
