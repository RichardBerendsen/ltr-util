import sys
import re


rqid = re.compile(r"qid:([^\s]+)")
rdocid = re.compile(r"docid:([^\s]+)")

path_preds = sys.argv[1]
f_preds = open(path_preds)
preds = [float(x.strip().split()[0]) for x in f_preds.readlines()]

qids = []
docids = []

path_feature_set = sys.argv[2]
f_feature_set = open(path_feature_set)
for l in f_feature_set:
	qids.append(re.search(rqid, l).group(1))
	docids.append(re.search(rdocid, l).group(1))


if len(set([len(preds), len(qids), len(docids)])) > 1:
	raise ValueError("Unequal number of predicitions, qids and docids")

for qid, docid, pred in zip(qids, docids, preds):
	print "%s\tQ0\t%s\t1\t%.12e\trunid" % (qid, docid, pred)

