from sparse_io import SparseSet
from qrel import Qrel
import sys

if len(sys.argv) != 3:
	print >> sys.stderr, "slap_class_labels_on_test_set.py qrel test_set"
	sys.exit(2)

qrel = Qrel(sys.argv[1]).binary()
test_set = SparseSet(sys.argv[2])

test_set_with_labels = SparseSet()

for q, (docids, labels, feats) in test_set.queries():

	# add the query
	test_set_with_labels.add_query(q)

	for docid, label, feat_vec in zip(docids, labels, feats):

		# find label
		if q in qrel and docid in qrel[q]:
			new_label = "1"
		else:
			new_label = "-1"

		# add document
		test_set_with_labels.add_document(q, docid, new_label, feat_vec)

test_set_with_labels.write()
