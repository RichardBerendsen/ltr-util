"""
%s < run

transform scores to reciprocal ranks.

Ties: all receive the same reciprocal rank: the rr of the top position.
A tie is observed when np.allclose() returns True.

So (simplified format, the real input format is a TREC run):

doc1 0.6
doc2 0.5
doc3 0.5

becomes:

doc1 1
doc2 0.5
doc3 0.5

In the input, we assume that a larger score is better.

We also assume that for each query the run is sorted by score, such that the
best results are on top.
"""

import sys
import getopt
import numpy as np
import collections

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "h")
    except getopt.GetOptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2


    if len(args) != 0:
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2

    for o, a in opts:
        if o == "-h":
            print __doc__ % (argv[0],)
            return 0
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)


    qid2last_score_rank = collections.defaultdict(lambda: [float("inf"), 0])
    SCORE = 0
    RANK = 1
    for l in sys.stdin:
        qid, docid, score = l.strip().split()
        score = float(score)
        if not np.allclose(score, qid2last_score_rank[qid][SCORE]):
            if score > qid2last_score_rank[qid][SCORE]:
                raise ValueError(\
                        "Error: Observed higher score further down the list")
            computed_rank = qid2last_score_rank[qid][RANK] + 1
            qid2last_score_rank[qid] = [score, computed_rank]
        else:
            computed_rank = qid2last_score_rank[qid][RANK]
        print "\t".join([qid, docid, "%.12e" % (1.0 / computed_rank,)])

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
