"""
%s feat

Normalize feat, subtracting mean and dividing by standard deviation.
Assume feat is a full feature vector (not a sparse one)

feat:
qid docid   x
...

Options:

    -q Normalize per query, instead of over all queries.
    -h Display this help

TODO: support multiple columns
"""
import getopt
import sys
import collections
import math

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "qh")
    except getopt.GetOptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2


    if len(args) != 1:
        print >> sys.stderr, __doc__ % (argv[0],)
        return 2

    per_query = False
    for o, a in opts:
        if o == "-h":
            print __doc__ % (argv[0],)
            return 0
        elif o == "-q":
            per_query = True
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)

    feat_path = args[0]

    if per_query:
        normalize_per_query(feat_path)
    else:
        normalize(feat_path)

    return 0


def normalize_per_query(feat_path): # {{{
    qid2stats = collections.defaultdict(lambda: {"N":0, "s":0.0, "s2":0.0, "x_":None, "ssd":None})
    # Go through file once for sum and sum of squares
    f = open(feat_path)
    for l in f:

        # ignore rank, work with x
        qid, docid, x = l.strip().split()
        x = float(x)

        if x < 0.0:
            print >> sys.stderr, "Error: Negative x in input, exiting"
            raise ValueError

        qid2stats[qid]["N"] += 1
        qid2stats[qid]["s"] += x
        qid2stats[qid]["s2"] += x * x

    for qid, stats in qid2stats.iteritems():
        stats["x_"] = stats["s"] / stats["N"]
        if stats["N"] > 1:
            stats["ssd"] = math.sqrt((stats["s2"] - \
                    stats["N"] * stats["x_"] ** 2) / \
                    (stats["N"] - 1))
        else:
            stats["ssd"] = float("inf")

    # Go through file a second time to normalize.
    f.seek(0)
    for l in f:
        qid, docid, x = l.strip().split()
        x = float(x)
        x = (x - qid2stats[qid]["x_"]) / \
                qid2stats[qid]["ssd"]

        print "\t".join([qid, docid, "%.12e" % (x,)])
#}}}
def normalize(feat_path): # {{{
    N = 0
    s = 0.0
    s2 = 0.0
    # Go through file once for N, sum and sum of squares
    f = open(feat_path)
    for l in f:

        # ignore rank, work with x
        qid, docid, x = l.strip().split()
        x = float(x)

        if x < 0.0:
            print >> sys.stderr, "Error: Negative x in input, exiting"
            raise ValueError

        N += 1
        s += x
        s2 += x * x

    if N:
        x_ = s / N
        if N > 1:
            ssd = math.sqrt((s2 - N * x_ * x_) / (N - 1))
        else:
            ssd = float("inf") # results in intuitive outcome: a single
                               # observation is zero standard deviations from
                               # the mean.

        # Go through file a second time to normalize.
        f.seek(0)
        for l in f:
            qid, docid, x = l.strip().split()
            x = float(x)
            x = (x - x_) / ssd

            print "\t".join([qid, docid, "%.12e" % (x,)])
#}}}

if __name__ == "__main__":
    sys.exit(main(sys.argv))
