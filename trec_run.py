"""
Class TrecRun
"""

from collections import OrderedDict
from operator import itemgetter
import re
import sys

class TrecRun:

	DOCS = 0
	SCORES = 1

	def __init__(self, fin = None, runid = None):
		self._q2docs_scores = OrderedDict()

		if runid is not None:
			self._runid = runid
		else:
			self._runid = "Unknown"

		if fin is not None:
			self.read(fin)


	def queries(self):
		return self._q2docs_scores.iteritems()

	def runid(self):
		return self._runid

	def read(self, fin):
		rws = re.compile(r"\s+")

		peek_runid = re.split(rws, fin.readline().strip())[5]
		if self._runid == "Unknown":
			self._runid = peek_runid
		fin.seek(0)

		for l in fin:
			qid, _, docid, _, score, runid = re.split(rws, l.strip())
			score = float(score)
			try:
				self._q2docs_scores[qid][TrecRun.DOCS].append(docid)
				self._q2docs_scores[qid][TrecRun.SCORES].append(score)
			except KeyError:
				self._q2docs_scores[qid] = ([docid], [score])

	"""
	Add query to run, supplying documents and corresponding scores

	Documents and scores will be stored sorted by score, and in case
	of ties, by docids (as does trec_eval).
	"""
	def add_query(self, qid, docs, scores):
		if qid in self._q2docs_scores:
			raise ValueError("qid %s already in run" % (qid,))
		docs_scores = zip(docs, scores)
		# http://docs.python.org/2/howto/sorting.html#sort-stability-and-complex-sorts
		docs_scores.sort(key=itemgetter(0))
		docs_scores.sort(key=itemgetter(1), reverse=True)
		docs, scores = zip(*docs_scores)
		self._q2docs_scores[qid] = (docs, scores)

	def query(self, qid):
		try:
			return self._q2docs_scores[qid]
		except KeyError:
			return []

	def docs(self, qid):
		try:
			return self._q2docs_scores[qid][TrecRun.DOCS]
		except KeyError:
			return []

	def scores(self, qid):
		try:
			return self._q2docs_scores[qid][TrecRun.SCORES]
		except KeyError:
			return []

	"""
	Cut off ranked lists for each query after rank k.
	"""
	def cutoff(self, k):
		for qid in self._q2docs_scores.keys():
			del self._q2docs_scores[qid][TrecRun.DOCS][k:]
			del self._q2docs_scores[qid][TrecRun.SCORES][k:]


	def write(self, fout):
		# trec_eval does not use rank anyway,
		# so let's set it to 1 everywhere, saves
		# some memory and computation time.
		for qid, (docs, scores) in self.queries():
			for docid, score in zip(docs, scores):
				print >> fout, "\t".join([qid, "Q0", docid, "1",
						"%.12e" % (score,), self.runid()])

def main(argv):

	if len(argv) != 2:
		print >> sys.stderr, "trec_run.py run"
		return 2

	f = open(sys.argv[1])
	run = TrecRun(f)
	f.close()
	run.write(sys.stdout)

	return 0

if __name__ == "__main__":
	sys.exit(main(sys.argv))
