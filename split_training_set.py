"""
Splits training set into training and validation set

Usage:

    split_training_set.py training_set_read training_set_write validation_set_write

Options:

    -p X
            Percentage of queries to use in validation set (default: twenty).
            Number of queries is floored.
    -q X
            Number of queries to use in validation set (default: not set).
            Overrides -p when set by the user.
    -r
            Sample number of validation queries randomly. If not set,
            validation queries are picked from the front of the order they are
            in in the training set.
    -h
            Display this help.
"""
from sparse_io import SparseSet
import sys
import math
import random
import getopt

def main(argv):
    try:
        opts, args = getopt.getopt(argv[1:], "q:p:rh")
    except getopt.GetOptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__
        return 2

    # default settings
    n_queries_set_by_user = None
    p_queries = 20.0
    sample_randomly = False

    for o, a in opts:
        if o == "-h":
            print __doc__
            return 0
        elif o == "-q":
            n_queries_set_by_user = int(a)
        elif o == "-p":
            p_queries = float(a)
        elif o == "-r":
            sample_randomly = True

    if len(args) != 3:
        print >> sys.stderr, "Wrong number of arguments"
        print >> sys.stderr, __doc__
        sys.exit(2)

    training_set = SparseSet(args[0])
    training_set_write = args[1]
    validation_set_write = args[2]
    validation_set = SparseSet()


    if n_queries_set_by_user is not None:
        n_queries = n_queries_set_by_user
    else:
        n_queries = int(math.floor((training_set.num_queries() * p_queries) / 100.0))

    qids = training_set.qids()
    if sample_randomly:
        validation_qid_idxs = random.sample(xrange(len(qids)), n_queries)
    else:
        validation_qid_idxs = range(n_queries)

    for i in validation_qid_idxs:
        validation_qid = qids[i]
        qid_value = training_set.pop_query(validation_qid)
        validation_set.add_query_with_value(validation_qid, qid_value)

    # check that training_set_write and validation_set_write do not exist yet
    try:
        open(training_set_write)
        print >> sys.stderr, "%s: File exists, not overwriting." % (training_set_write,)
    except IOError:
        f = open(training_set_write, 'w')
        training_set.write(f)
        f.close()
    try:
        open(validation_set_write)
        print >> sys.stderr, "%s: File exists, not overwriting." % (validation_set_write,)
    except IOError:
        f = open(validation_set_write, 'w')
        validation_set.write(f)
        f.close()

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
