"""
normalize_per_query.py feature_set
"""

import sys
from sparse_io import SparseSet

if len(sys.argv) != 2:
	print >> sys.stderr, __doc__
	sys.exit(2)

# instantiate a feature set with support for normalization.
training_set = SparseSet(sys.argv[1], True)
training_set.normalize_linear(True)
training_set.write()
